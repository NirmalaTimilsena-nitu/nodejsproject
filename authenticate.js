var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/user');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

var config = require('./config.js');


exports.Local = passport.use(new LocalStrategy(User.authenticate()));
//User.authenticate() that will provide the authentication  local Startegy
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

exports.getToken = function (user) {
    return jwt.sign(user, config.secretKey,
        { expiresIn: 3600 });
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.secretKey;

exports.jwtPassport = passport.use(new JwtStrategy(opts,
    (jwt_payload, done) => {
        console.log("JWT payload: ", jwt_payload);
        User.findOne({ _id: jwt_payload._id }, (err, user) => {
            if (err) {
                return done(err, false);
            }
            else if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    }));
exports.verifyOrdinaryUser = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config["secret-key"], function (err, decoded) {
            if (err) {
                var err = new Error('Not authenticated!');
                err.status = 401;
                return next(err);
            }
            else {
                req.decoded = decoded;
                next();
            }
        });
    }
    else {
        var err = new Error('No token provided!');
        err.status = 403;
        return next(err);
    }
}

exports.verifyAdmin = function verifyAdmin(auth, res, next) {
    // console.log(auth);
    if (req.user.admin === true) {
        return next()
    }
    else {
        var err = new Error("You're not authorized for this operation!");
        err.status = 403;
        return next(err);
    };
};

// exports.verifyAdmin = verifyAdmin;
exports.verifyUser = passport.authenticate('jwt', { session: false });